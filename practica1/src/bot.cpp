#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"

int main(int argc, char* argv[])
{

	int file;
	DatosMemCompartida* memcomp_ptr;
	char* proyeccion;
	

	file=open("/tmp/DatosBot",O_RDWR);
	

	proyeccion=(char*)mmap(NULL,sizeof(*(memcomp_ptr)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);
	

	close(file);
	

	memcomp_ptr=(DatosMemCompartida*)proyeccion;

	
	int salir=0;

	
	while(salir==0)
	{
		if (memcomp_ptr->accion==3)
		{
			salir=1;
		}
		float posRaqueta;
		posRaqueta=((memcomp_ptr->raqueta1.y2+memcomp_ptr->raqueta1.y1)/2);
		
		if(posRaqueta<(memcomp_ptr->esfera.centro.y))
			memcomp_ptr->accion=1;

		if(posRaqueta>(memcomp_ptr->esfera.centro.y))
			memcomp_ptr->accion=-1;
		else
			memcomp_ptr->accion=0;
		
		usleep(2500);
	}
	

	munmap(proyeccion,sizeof(*(memcomp_ptr)));
	return 0;

}
